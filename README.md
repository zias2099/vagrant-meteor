# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

Getting MeteorJs to work
see https://gist.github.com/gabrielhpugliese/5855677
https://www.youtube.com/watch?v=woFUR1dMZ3g

#vagrant ssh into guest
#create meteor app in guest's home 
vagrant@meteor-dev:~$ cd /home/vagrant 
or vagrant@meteor-dev:~$ cd ~

vagrant@meteor-dev:~$ meteor create ~/todos
#then copy the newly created app into the shared vagrant folder
vagrant@meteor-dev:/vagrant$ cp -r /home/vagrant/todos /vagrant

# ls to verify the results of the copy
vagrant@meteor-dev:/vagrant$ ls /vagrant
meteor.sh provision.sh todos Vagrantfile 

#do some magic then meteor will work
vagrant@meteor-dev:/vagrant$ sudo mount --bind /home/vagrant/todos/.meteor/ /vagrant/todos/.meteor/
vagrant@meteor-dev:/vagrant$ echo "sudo mount --bind /home/vagrant/todos/.meteor/ /vagrant/todos/.meteor/" >> ~/.bashrc

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact