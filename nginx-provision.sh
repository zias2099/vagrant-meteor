apt-get -y update

apt-get -y install nginx

rm -rf /usr/share/nginx/html
ln -s /vagrant/www /usr/share/nginx/html

service nginx start